import yaml
import django
import argparse

django.setup()

from django import db
from django.core import management

from dcomputationaltool.models import ComputationalTool, ComputationalWF

from dcomputationaltool.lib.gitlab import GitLabFactory
from dcomputationaltool.lib.computationaltools import insert_computationaltool, insert_computationalwf

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Parse and insert a CWL project from Gitlab')
    parser.add_argument('-m', help='Migrate the dcomputationaltool app before inserting', action='store_true',
                        required=False,
                        default=False)
    parser.add_argument('--project_id', help='Gitlab project ID', required=True)
    parser.add_argument('--config_file', help='Gitlab API config file', required=True)
    parser.add_argument('--gitlab_id', help='Gitlab section name on the Gitlab API config file', required=True)
    parser.add_argument('--branch', help='Git branch name to process. Default: master', required=False)

    print("Using database: %s on Host: %s as user: %s" % (
        db.connections.databases['default']['NAME'],
        db.connections.databases['default']['HOST'],
        db.connections.databases['default']['USER']
    ))

    args = parser.parse_args()
    migrate = args.m
    project_id = args.project_id
    config_file = args.config_file
    gitlab_id = args.gitlab_id
    branch = 'master' if not args.branch else args.branch

    if migrate:
        management.call_command("makemigrations", "dcomputationaltool")
        management.call_command("migrate", "dcomputationaltool")

    gl_factory = GitLabFactory(project_id, gitlab_id, config_file)
    gl_factory.load_cwl_data(branch)

    print('Inserting the tools')
    insert_computationaltool(gl_factory)
    print('Tools inserted: %d' % ComputationalTool.objects.all().count())

    print('Inserting the workflows')
    insert_computationalwf(gl_factory)
    print('Workflows inserted: %d' % ComputationalWF.objects.all().count())
