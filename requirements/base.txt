uWSGI~=2.0.17
Django~=2.1
django-appconf~=1.0
django-compressor~=2.2
django-filter~=2.1
djangorestframework~=3.9
settings-overrider~=0.5
urllib3~=1.24
python-gitlab~=1.7

